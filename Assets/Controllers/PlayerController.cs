﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public float jumpHeight;
	private bool collided;

	// Use this for initialization
	void Start ()
	{
		this.collided = false;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {
			GetComponent<Rigidbody2D> ().AddForce(new Vector2 (0, jumpHeight * Time.deltaTime), ForceMode2D.Impulse);
		}
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		this.collided = true;
	}

	public void selfDestroy()
	{
		if (this.gameObject != null) {
			Destroy(this.gameObject);
		}
	}

	public bool getCollided()
	{
		return this.collided;
	}
}
