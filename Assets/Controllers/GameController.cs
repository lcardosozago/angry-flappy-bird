﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour {
    public GameObject hudCanvas;
	public GameObject scoreTextObject;
	public GameObject player;
    public float restartDelay = 5f;

    Text scoreText;

    Animator anim;
    float restartTimer;

    int score;
    bool gameOver;

    PlayerController playerController;

    void Awake()
    {
        // Set up the reference.
        anim = hudCanvas.GetComponent<Animator>();

        scoreText = scoreTextObject.GetComponent<Text>();
    }

    // Use this for initialization
    void Start () {
		this.score = 0;
		this.gameOver = false;

		this.playerController = this.player.GetComponent<PlayerController> ();

		UpdateScore ();
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameOver) {
            anim.SetTrigger("GameOver");
            
            restartTimer += Time.deltaTime;
            
            if (restartTimer >= restartDelay)
            {
                Application.LoadLevel(Application.loadedLevel);
            }
		} else {
            this.AddScore(Random.Range(1, 5));
        }

        if (this.player != null) {
			if (this.player.transform.position.y < -5 || this.player.transform.position.y > 5 || this.playerController.getCollided ()) {
				this.gameOver = true;
				this.playerController.selfDestroy ();
			}
		}
	}

	private void AddScore (int newScoreValue) {
		this.score += newScoreValue;
		UpdateScore ();
	}

	private void UpdateScore ()
    {
        this.scoreText.text = "Score: " + this.score;
    }
}
