﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
	public float speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.x < -8.5f) {
			if (this.gameObject != null) {
				Destroy (this.gameObject);
			}
		}

		transform.Translate(Vector3.left * this.speed * Time.deltaTime);
	}
}
