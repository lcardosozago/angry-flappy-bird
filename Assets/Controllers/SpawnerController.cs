﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnerController : MonoBehaviour {
	public float minHeight;
	public float maxHeight;
	public float rateSpawn;
	public int secondsToIncreaseDifficulty;
	public List<GameObject> prefabs;

	// Use this for initialization
	void Start ()
	{
		Invoke ("CreateEnemy", this.rateSpawn);
		InvokeRepeating ("IncreaseDifficulty", 5f, this.secondsToIncreaseDifficulty);
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}

	private void CreateEnemy ()
	{
		GameObject prefab = prefabs[Random.Range( 0, this.prefabs.Count )];

		Instantiate (prefab, new Vector3(transform.position.x, Random.Range(this.minHeight, this.maxHeight), transform.position.z), Quaternion.identity);

		prefab.SetActive(true);

		Invoke ("CreateEnemy", this.rateSpawn);
	}

	private void IncreaseDifficulty ()
	{
		if (this.rateSpawn > 0.8f) {
			this.rateSpawn -= 0.3f;
		}
	}
}
